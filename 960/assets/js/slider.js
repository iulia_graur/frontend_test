// On page load.
$(function() {
// List element.
var $list = $('#list');

// Populate list with items.
var populateList = function() {
    // List items.
    var items = [
        'Web Design', 'UI / UX Consulting', 'App Development', 'Programmer', 'Web Design', 'UI / UX Consulting', 'App Development', 'Programmer'
    ];

    // Index of current element starting from 0. If list has 5 elements, current element is the third (index 2).
    var currentIndex = 1; 

    // Populate list.
    $.each(items, function(index, item){
        var current = '';

        // Set class for current item.
        if (index === currentIndex){
            current = 'current';
        }

        // Append item.
        $('.options .slider', $list).append(
            '<div class="option ' + current + '">' + item + '</div>'
        );
    });
};

// Handle list events.
var handleEvents = function() {
    // Set arrows click handlers.
    $list.on('click', '.arrow', function(){
        var $arrow = $(this);
        var $slider = $('.options .slider', $list);
        var currentMargin = parseInt($slider.css('margin-top'));
        var itemHeight = $('.option', $list).height();
        var $currentItem = $('.option.current', $list);
        var visibleItems = 3;
        var maxMargin = ($('.option', $list).length - 1 - visibleItems) * itemHeight;

        // Identify which arrow clicked.
        if ($arrow.is('.up')) {
            // Up arrow clicked.

            // Do not move if no items available.
            if (currentMargin < -1 * maxMargin) {
                return;
            }

            // Move list up.
            $slider.animate({
               marginTop:  currentMargin - itemHeight
            }, 'fast');

            // Move current marker to next element.
            var $nextItem = $currentItem.next('.option');
            $currentItem.removeClass('current');
            $nextItem.addClass('current');
        } else if ($arrow.is('.down')) {
            // Down arrow clicked.

            // Do not move if no items available.
            if (currentMargin > -1 * itemHeight) {
                return;
            }

            // Move list down.
            $slider.animate({
               marginTop:  currentMargin + itemHeight
            }, 'fast');

            // Move current marker to prev element.
            var $prevItem = $currentItem.prev('.option');
            $currentItem.removeClass('current');
            $prevItem.addClass('current');
        }
    });
};

// Execute plugin.
populateList();
handleEvents();
});
